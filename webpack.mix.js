let mix = require('laravel-mix');
const config = require('./webpack.config');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig(config);

// mix.webpackConfig({
//     resolve: {
//         extensions: ['.js', '.vue', '.json'],
//         alias: {
//             'vue$': 'vue/dist/vue.esm.js',
//             '@pages': __dirname + '/resources/js/pages',
//             '@components': __dirname + '/resources/js/components'
//         },
//     },
// });

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

if (mix.inProduction()) {
    mix.version();

    mix.extract([
        'vue',
        'axios',
        'vuex',
        'jquery',
        'bootstrap',
        'vue-router',
        'vue-sweetalert2',
    ])
}

mix.webpackConfig({
    output: {
        chunkFilename: 'js/[name].[chunkhash].js',
    },
});
