<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/test', 'TestController');

Route::group([ /*'middleware' => [ 'web' ], /* */ 'as' => 'ajax.', 'prefix' => '/ajax'], function () {
    Route::resource('machine', 'AjaxMachineController');
    Route::any('machine/{machine}/history', 'AjaxMachineController@history');

    Route::any('machine/{machine}/photo', 'AjaxMachineController@photo');
    Route::any('machine/{machine}/statistic', 'StatisticController');

    Route::any('machine/data/{uuid}', 'AjaxMachineController@postData');
});

Route::any('{all}', function () {
    return view('welcome');
})->where('all', '.*');
