<?php

namespace App\Models;

use App\Models\HistoryMachine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Machine
 *
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Machine onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Machine withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Machine withoutTrashed()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $deleted_at
 * @property string $name
 * @property string $description
 * @property string $connect
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\HistoryMachine[] $histories
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereConnect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereUpdatedAt($value)
 * @property int $empty_weight
 * @property int $full_weight
 * @property string $location
 * @property string $type
 * @property string $uuid
 * @property int $volume
 * @property-read array $statday_week
 * @property-read \App\Models\HistoryMachine $lasthist
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Statday[] $statday
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereEmptyWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereFullWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Machine whereVolume($value)
 */
class Machine extends Model
{
    use SoftDeletes;

    protected $table = 'machines';

    protected $fillable = [
        'name',
        'description',
        'connect',
        'location',
        'type',
        'volume',
        'empty_weight',
        'full_weight',
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'connect' => 'string',
        'location'=> 'string',
        'uuid'=> 'string',
        'type'=> 'string',
        'volume'=> 'integer',
        'empty_weight'=> 'integer',
        'full_weight'=> 'integer',
    ];

    public function histories()
    {
        return $this->hasMany(HistoryMachine::class, 'machine_id', 'id');
    }

    public function lasthist()
    {
        return $this->hasOne(HistoryMachine::class, 'machine_id', 'id')->orderBy('created_at', 'desc');
    }

    public function statday()
    {
        $query = $this->hasMany(Statday::class, 'machine_id', 'id');

        $timeStart = date('H') * 60 + date('i');
        $query->getBaseQuery()->select([
            'machine_id',
            \DB::raw('WEEKDAY(created_at) AS dayofweek'),

            \DB::raw("DATE_FORMAT(DATE_ADD('2000-01-01', INTERVAL (DATE_FORMAT(created_at,'%H') * 60 + (DATE_FORMAT(created_at,'%i') DIV 5 * 5)) MINUTE), '%H:%i' ) AS `time`"),
            \DB::raw("(DATE_FORMAT(created_at,'%H') * 60 + (DATE_FORMAT(created_at,'%i') DIV 5 * 5)) AS h"),
            \DB::raw('AVG(result) AS r_avg'),
            \DB::raw('SUM(result) AS r_sum'),
            \DB::raw('COUNT(result) AS r_count'),
        ])
            ->groupBy(['machine_id', 'dayofweek', 'h', 'time'])
            ->orderBy('h')
            ->having(\DB::raw('`dayofweek`'), '=', date('w'))
            ->having(\DB::raw('`h`'), '>=', $timeStart)
            ->having(\DB::raw('`h`'), '<=', $timeStart + 60)
        ;

        return $query;
    }

    /**
     * @return array
     */
    public function getStatdayWeekAttribute()
    {
        $dayofweek = date('w');
        $default = collect();
        for ($i = 0; $i < 1440; $i+=5) {
            $item = [
                "h" => $i,
                "dayofweek" => $dayofweek,
                "r_avg" => 0,
                "r_sum" => 0,
                "r_count" => 0,
                "time"=> \Carbon\Carbon::createFromFormat('U', $i * 60)->format('H:i'),
            ];
            $default[(string) $i] = $item;
        }

        collect($this->getRelationValue('statday'))->map(function ($v) use ($default) {
            $default[$v->h] = $v;
            return $v;
        })->keyBy('h');

        return $default->map(function ($v) {
            return [
                'time' => $v['time'],
                'avg' => $v['r_avg'],
            ];
        })->values();
    }
}
