<?php

namespace App\Models;

use App\Models\HistoryMachine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Statday
 *
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $id
 * @property int $machine_id
 * @property string $name
 * @property int $result
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereMachineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Statday whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Statday extends Model
{
    protected $table = 'histories_machine';

    protected $fillable = [
    ];

    protected $casts = [
        'machine_id' => 'integer',
        'dayofweek' => 'integer',
        'time' => 'string',
        'h' => 'integer',
        'r_avg' => 'integer',
        'r_sum' => 'integer',
        'r_count' => 'integer',
    ];
}
