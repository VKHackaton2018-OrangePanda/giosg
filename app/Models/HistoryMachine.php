<?php

namespace App\Models;

use App\Models\Machine;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\HistoryMachine
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $machine_id
 * @property int $volume_percentage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Machine $machine
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryMachine onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereMachineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereVolumePercentage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryMachine withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\HistoryMachine withoutTrashed()
 * @property string $name
 * @property int $result
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\HistoryMachine whereResult($value)
 */
class HistoryMachine extends Model
{
    // use SoftDeletes;

    protected $table = 'histories_machine';

    protected $fillable = [
        'created_at',
        'updated_at',
        'machine_id',
        'volume_percentage',
        'name',
        'result',
    ];

    protected $casts = [
        'id' => 'integer',
        'machine_id' => 'integer',
        'volume_percentage' => 'integer',
        'name' => 'string',
        'result' => 'integer',
    ];


    public function machine()
    {
        return $this->belongsTo(Machine::class, 'machine_id', 'id');
    }
}
