<?php

namespace App\Http\Controllers;

use App\Models\Machine;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
    public function __invoke(Request $request, Machine $machine)
    {
        $whereDayofweek = $request->has('w') ? ((int) $request->get('w')) : null;
        $dm = $request->has('dm') ? ((int) $request->get('dm')) : 5;

        $default = collect();
        for ($i = 0; $i < 10080; $i+=$dm) {
            $dayofweek = (int) floor($i / 1440);
            if ($whereDayofweek !== null && $whereDayofweek !== $dayofweek) {
                continue;
            }
            $item = [
                "h" => $i,
                "dayofweek"=> $dayofweek,
                "r_avg"=> 0,
                "r_sum"=> 0,
                "r_count"=> 0,
                "time"=> \Carbon\Carbon::createFromFormat('U', ($i - ($dayofweek * 1440)) * 60)->format('H:i'),
            ];
            $default[(string) $i] = $item;
        }
        // ------------------

        $whereDayofweek = ($whereDayofweek !== null) ? "AND $whereDayofweek" : "";
        $sql = <<<SQL
SELECT machine_id,
        WEEKDAY(created_at) AS dayofweek,
              DATE_FORMAT(
        DATE_ADD('2000-01-01',
          INTERVAL (DATE_FORMAT(created_at,'%H') * 60 + (DATE_FORMAT(created_at,'%i') DIV $dm * $dm)) MINUTE
           ), '%H:%i'
         ) AS `time`,
       (DATE_FORMAT(created_at,'%H') * 60 + (DATE_FORMAT(created_at,'%i') DIV $dm * $dm)) AS h,
--        (WEEKDAY(created_at) * 1440) + (DATE_FORMAT(created_at,'%H') * 60 + (DATE_FORMAT(created_at,'%i') DIV 5 * 5)) AS h,
      AVG(result) AS r_avg,
      SUM(result) AS r_sum,
      COUNT(result) AS r_count

FROM histories_machine
WHERE machine_id = ? {$whereDayofweek}
GROUP BY machine_id, dayofweek, h, time
ORDER BY h;
SQL;

        $result = \DB::connection()->select(\DB::raw($sql), [$machine->id]);
        /** @var \Illuminate\Support\Collection $result */
        collect($result)->map(function ($v) use ($default) {
            $v->h = (int) $v->h;
            $v->r_avg = (int) $v->r_avg;
            $v->r_sum = (int) $v->r_sum;


            $v->dayofweek = (int) $v->dayofweek; // (int) floor($v->h / 1440);

            $time = Carbon::createFromFormat('Y-m-d H:i:s', '2018-01-01 00:00:00');
            $time = $time->addMinute($v->h - ($v->dayofweek * 1440));
            $v->time = $time->format('H:i');

            $default[$v->h] = $v;

            return $v;
        })->keyBy('h');

        return $default->values();
    }
}
