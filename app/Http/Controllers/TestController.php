<?php

namespace App\Http\Controllers;

use App\Models\Machine;
use Illuminate\Http\Request;

/**
 * Class TestController
 * @package App\Http\Controllers
 */
class TestController extends Controller
{
    public function __invoke()
    {
        $machines = Machine::all();
        $machines->load('statday');
        // $machines->each->append('statday_week');
        // $machines->each->addHidden('statday');

        // dump($machines->first()->addstatday_full);
        dump($machines->toArray());
        // $model = Machine::find(1);
        // $model->statday;
        // dump($model);
        return '1';
}
}
