<?php

namespace App\Http\Controllers;

use App\Models\HistoryMachine;
use App\Models\Machine;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AjaxMachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $machines = Machine::with('statday', 'lasthist')
            // ->append('statday_week')
            ->paginate();
        // $machines->appends('statday_week');

        return $machines;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Machine();
        return view('example.machine.edit', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Machine($request->all());
        $model->uuid = str_random();
        $model->save();
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Machine::findOrFail($id);
        return $model;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Machine::findOrFail($id);

        return view('example.machine.edit', ['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Machine::findOrFail($id);
        $model->fill($request->all());
        $model->save();
        return $model;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Machine::find($id)->delete();
    }

    public function history(Request $request, Machine $machine)
    {
        return HistoryMachine::where('machine_id', '=', $machine->id)
            ->orderBy('created_at', 'desc')
            ->paginate(1000);
    }

    public function photo(Request $request, Machine $machine)
    {
        $files = [
            'camera-0-20181018-122901.jpg',
            'camera-0-20181018-123001.jpg',
            'camera-0-20181018-123021.jpg',
            'camera-0-20181018-123038.jpg',
            'camera-0-20181018-123039.jpg',
            'camera-0-20181018-123040.jpg',
            'camera-0-20181018-123053.jpg',
            'camera-0-20181018-123101.jpg',
            'camera-0-20181018-123102.jpg',
            'camera-0-20181018-123104.jpg',
            'camera-0-20181018-123105.jpg',
            'camera-0-20181018-123114.jpg',
            'camera-0-20181018-123120.jpg',
            'camera-0-20181018-123125.jpg',
            'camera-0-20181018-123127.jpg',
            'camera-0-20181018-123146.jpg',
            'camera-0-20181018-123147.jpg',
            'camera-0-20181018-123201.jpg',
            'camera-0-20181018-123225.jpg',
            'camera-0-20181018-123226.jpg',
            'camera-0-20181018-123241.jpg',
            'camera-0-20181018-123257.jpg',
            'camera-0-20181018-123301.jpg',
            'camera-0-20181018-123401.jpg',
            'camera-0-20181018-123501.jpg',
            'camera-0-20181018-123601.jpg',
            'camera-0-20181018-123701.jpg',
            'camera-0-20181018-123801.jpg',
            'camera-0-20181018-123901.jpg',
            'camera-0-20181018-124001.jpg',
            'camera-0-20181018-124101.jpg',
            'camera-0-20181018-124201.jpg',
            'camera-0-20181018-124301.jpg',
            'camera-0-20181018-124401.jpg',
            'camera-0-20181018-124501.jpg',
            'camera-0-20181018-124601.jpg',
            'camera-0-20181018-124701.jpg',
            'camera-0-20181018-124801.jpg',
            'camera-0-20181018-124901.jpg',
            'camera-0-20181018-125001.jpg',
            'camera-0-20181018-125101.jpg',
            'camera-0-20181018-125201.jpg',
            'camera-0-20181018-125301.jpg',
            'camera-0-20181018-125401.jpg',
            'camera-0-20181018-125501.jpg',
            'camera-0-20181018-125601.jpg',
            'camera-0-20181018-125701.jpg',
            'camera-0-20181018-125801.jpg',
            'camera-0-20181018-125901.jpg',
            'camera-0-20181018-130001.jpg',
            'camera-0-20181018-130101.jpg',
            'camera-0-20181018-130201.jpg',
            'camera-0-20181018-130301.jpg',
            'camera-0-20181018-130401.jpg',
            'camera-0-20181018-130501.jpg',
            'camera-0-20181018-130601.jpg',
            'camera-0-20181018-130701.jpg',
            'camera-0-20181018-130801.jpg',
            'camera-0-20181018-130901.jpg',
            'camera-0-20181018-131001.jpg',
            'camera-0-20181018-131101.jpg',
            'camera-0-20181018-131201.jpg',
            'camera-0-20181018-131301.jpg',
            'camera-0-20181018-131402.jpg',
            'camera-0-20181018-131501.jpg',
        ];
        $jpg = sprintf('camera-0-20181018-13%s.jpg', date('is'));
        $jpg = sprintf('camera-0-20181018-13%s.jpg', date('i01'));


        if (!in_array($jpg, $files)) {
            $jpg = array_first($files);
        }
        $file = base_path('tmp/camera-0/' . $jpg);

        // $file = base_path('tmp/camera-0/camera-0-20181018-123105.jpg');
        return response()->file($file);
    }

    public function postData(Request $request, $uuid)
    {
        $machine = Machine::whereUuid($uuid)->first();
        /** @var Machine $machine */

        $current_weight = (int) $request->get('current_weight') - $machine->empty_weight;
        if ($current_weight < 0) {
            $current_weight = 0;
        }

        $result = ($current_weight / ($machine->full_weight - $machine->empty_weight)) * 100;
        if ($result > 100) {
            $result = 100;
        }

        $history = new HistoryMachine();
        $history->machine_id = $machine->id;
        $history->name = '-';
        $history->result = (int) $result;

        $history->save();
        return 1;
    }
}
