import _thread
import serial
import json
import requests

# const SERVER_URI
SERVER_URI = "https://giosg.orangepanda.ru/ajax/machine/data/"
#SERVER_URI = "http://localhost:8000/ajax/machine/data/"
SERIAL_PORT = serial.Serial("/dev/cu.usbmodem1411", 9600, timeout=2)


# initial reading from com port
def comPortInitial():
    print(SERIAL_PORT.name)


# read from com and send to url
def sendFromSerial():

    ch = SERIAL_PORT.readline().decode()
    if (ch.__len__() > 0 & ch.__len__() < 3):
        res = json.loads(ch)
        # count in american spoons need multiply to 4.9
        res['current_weight'] = round(res['current_weight'] * 4.9, 2)
        uuid = res['uuid']
        # counter = res['counter']
        payload = res

        print(payload)
        try:
            #request sender
            r = requests.get(SERVER_URI + uuid, params=payload)
            r.raise_for_status()
        except requests.ConnectionError as e:
            print("OOPS!! Connection Error. Make sure you are connected to Internet. Technical Details given below.\n")
            print(str(e))
        except requests.Timeout as e:
            print("OOPS!! Timeout Error")
            print(str(e))
        except requests.RequestException as e:
            print("OOPS!! General Error")
            print(str(e))
        except KeyboardInterrupt:
            print("Someone closed the program")


def requesterDaemon():
    while True:
        sendFromSerial()


if __name__ == "__main__":
    comPortInitial()
    requesterDaemon()
