const path = require('path');

module.exports = {
    resolve: {
        extensions: ['.js', '.json', '.vue'],
        alias: {
            '@pages': path.resolve(__dirname + '/resources/js/pages'),
            '@components': path.resolve(__dirname + '/resources/js/components'),
            '@': path.resolve(__dirname, './resources/js')
        }
    },
};
