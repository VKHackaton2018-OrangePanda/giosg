<?php
/** @var \App\Models\Machine $model */
?>
@extends('example.layout')

@section('content')

    <form method="post" action="{{ $model->id ? route('ajax.machine.update') : route('ajax.machine.update', ['machine' => $model->id])}}">
        @if($model->id) @method('PUT')
        @else @method('POST')
        @endif

        @csrf

        <h1>Machine</h1>
        <div class="form-group row">
            <label for="staticName" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" required id="staticName" name="name" value="{{$model->name}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputDescription" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" required id="inputDescription" name="description" value="{{$model->description}}">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputUrl" class="col-sm-2 col-form-label">Url</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" required id="inputUrl" name="connect" value="{{$model->connect}}">
            </div>
        </div>

            <div class="form-group row">
                <label for="inputLocation" class="col-sm-2 col-form-label">location</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" required id="inputLocation" name="location" value="{{$model->location}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputType" class="col-sm-2 col-form-label">Type</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" required id="inputType" name="type" value="{{ $model->type ?? 'machine'}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="inputVolume" class="col-sm-2 col-form-label">Volume</label>
                <div class="col-sm-10">
                    <input type="number" min="0" step="1" class="form-control" required id="inputVolume" name="volume" value="{{$model->volume}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="input_empty_weight" class="col-sm-2 col-form-label">empty_weight</label>
                <div class="col-sm-10">
                    <input type="number" min="0" step="1" class="form-control" required id="input_empty_weight" name="empty_weight" value="{{$model->empty_weight}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="input_full_weight" class="col-sm-2 col-form-label">full_weight</label>
                <div class="col-sm-10">
                    <input type="number" min="0" step="1" class="form-control" required id="input_full_weight" name="full_weight" value="{{$model->full_weight}}">
                </div>
            </div>

        <button type="submit" class="btn btn-primary my-1">Submit</button>
    </form>

@endsection
