/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueSweetalert2 from 'vue-sweetalert2';
import VueNoty from 'vuejs-noty'

Vue.use(VueSweetalert2);
Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueNoty);

Vue.component('base-layout', require('./components/BaseLayout.vue'));

const routes = [
    // {path: '/', component: () => import('./pages/ApplicationComponent.vue'), name: 'applicationPage'},
    {path: '/', component: () => import('./pages/Machines.vue'), name: 'machines'},
    {path: '/machine/new', component: () => import('./pages/PotNew.vue'), name: 'potNew'},
    {path: '/machine/:id', component: () => import('./pages/PotManage.vue'), name: 'potManage'},
    {path: '/machine/:id/analytic', component: () => import('./pages/Analytics.vue'), name: 'analytics'},

    /* Error pages */
    {path: '/401', component: () => import('./components/errors/401'), name: 'page401'},
    {path: '/403', component: () => import('./components/errors/403'), name: 'page403'},
    {path: '/419', component: () => import('./components/errors/419'), name: 'page419'},
    {path: '/429', component: () => import('./components/errors/429'), name: 'page429'},
    {path: '/500', component: () => import('./components/errors/500'), name: 'page500'},
    {path: '/503', component: () => import('./components/errors/503'), name: 'page503'},
    {path: '/404', component: () => import('./components/errors/404'), name: 'page404', alias: '*'},
];

const router = new VueRouter({
    mode: 'history',
    routes
});

const store = new Vuex.Store({
    components: {},
    state: {},
    mutations: {}
});

new Vue({
    router,
    store,
}).$mount('#app');
