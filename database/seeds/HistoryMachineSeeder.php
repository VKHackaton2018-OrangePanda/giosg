<?php

use App\Models\HistoryMachine;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HistoryMachineSeeder extends Seeder
{
    public function run()
    {
        DB::connection()->statement('truncate table histories_machine;');
        $list = json_decode(file_get_contents(__DIR__ . '/history.json'), true);

        DB::beginTransaction();
        foreach ($list as $value) {
            // "name":"camera-1-20181029-112403.jpg","result":79
            preg_match('/camera-(?<machine>\d+)-(?<date>.+?)\.jpg/', $value['name'], $m);

            $machine_id = $m['machine'] + 1;
            $date = Carbon::createFromFormat('Ymd-His', $m['date'])->addDays('5');

            HistoryMachine::create([
                'machine_id' => $machine_id,
                'created_at' => $date,
                'updated_at' => $date,
                'name' => $value['name'],
                'result' => $value['result'],
            ]);
        }
        DB::commit();
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run1()
    {
        DB::connection()->statement('truncate table histories_machine;');

        $start = Carbon::createFromDate(2018, 11, 01);
        $start->second(1);
        $now = Carbon::now();

        $volumePercentage = rand(0, 100);

        DB::beginTransaction();
        $i = 0;

        while ($start < $now) {
            $start->addMinute(1);
            if ($volumePercentage > 0 && (int) (rand(0, 100) / 80)) {
                $volumePercentage--;
            }

            $startWork = Carbon::createFromFormat('Y-m-d H:i:s', $start->format('Y-m-d 08:00:00'));

            if ($start < $startWork) {
                continue;
            }
            if ($start->format('H:i') == '08:00') {
                $volumePercentage = 100;
                continue;
            }

            HistoryMachine::create([
                'machine_id' => 1,
                'created_at' => $start,
                'updated_at' => $start,
                'volume_percentage' => $volumePercentage,
            ]);

            if ($i++ > 1000) {
                DB::commit();
                DB::beginTransaction();
                $i = 0;
                echo "commit 1000\n";
            }
            DB::commit();
        }

    }
}
