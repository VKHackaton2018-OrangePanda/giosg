<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Machine::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text(150),
        'connect' => $faker->url,
    ];
});
